package com.rca.rca_stock_mis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RcaStockMisApplication {

	public static void main(String[] args) {
		SpringApplication.run(RcaStockMisApplication.class, args);
	}

}
