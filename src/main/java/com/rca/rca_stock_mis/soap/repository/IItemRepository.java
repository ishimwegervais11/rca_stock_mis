package com.rca.rca_stock_mis.soap.repository;

import com.rca.rca_stock_mis.soap.bean.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IItemRepository extends JpaRepository<Item,Long> {
}
