package com.rca.rca_stock_mis.soap.repository;

import com.rca.rca_stock_mis.soap.bean.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ISupplierRepository extends JpaRepository<Supplier,Long> {
}
