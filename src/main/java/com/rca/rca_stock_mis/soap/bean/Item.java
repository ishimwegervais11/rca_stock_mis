package com.rca.rca_stock_mis.soap.bean;

import com.rca.rca_stock_mis.soap.enums.StatusEnum;
import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
@Table
public class Item {
    @Id
    @GeneratedValue
    private long id;
    @NotNull
    private String name;
    @NotNull
    private String itemCode;
    @Enumerated(EnumType.STRING)
    private StatusEnum status;
    @NotNull
    private Double price;
    @OneToOne
    private Supplier supplier;

public Item(){}
    public Item(long id, String names, String itemCode, StatusEnum status, Double price, Supplier supplier) {
        this.id = id;
        this.name = names;
        this.itemCode = itemCode;
        this.status = status;
        this.price = price;
        this.supplier = supplier;
    }

    public int getId() {
        return (int) id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String names) {
        this.name = names;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }
}
