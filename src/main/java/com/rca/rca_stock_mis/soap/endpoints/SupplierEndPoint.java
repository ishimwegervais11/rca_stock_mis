package com.rca.rca_stock_mis.soap.endpoints;

import com.rca.rca_stock_mis.soap.bean.Supplier;
import com.rca.rca_stock_mis.soap.repository.ISupplierRepository;
import com.rca.rca_stock_mis.suppliers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;
import java.util.Optional;
@Endpoint
public class SupplierEndPoint {
    
    @Autowired
    private ISupplierRepository supplierRepository;

    @PayloadRoot(namespace = "http://rca_stock_mis.rca.com/suppliers", localPart = "GetSupplierDetailsRequest")
    @ResponsePayload
    public GetSupplierDetailsResponse findById(@RequestPayload GetSupplierDetailsRequest request) {

        Supplier supplier = supplierRepository.findById((long) request.getId()).get();

        GetSupplierDetailsResponse SupplierDetailsResponse = mapSupplierDetails(supplier);
        System.out.println(SupplierDetailsResponse);
        return  SupplierDetailsResponse;
    }

    @PayloadRoot(namespace = "http://rca_stock_mis.rca.com/suppliers", localPart = "GetAllSupplierDetailsRequest")
    @ResponsePayload
    public GetAllSupplierDetailsResponse findAll(@RequestPayload GetAllSupplierDetailsRequest request){
        GetAllSupplierDetailsResponse allSupplierDetailsResponse = new GetAllSupplierDetailsResponse();

        List<Supplier> suppliers = supplierRepository.findAll();
        for (Supplier supplier: suppliers){
            GetSupplierDetailsResponse SupplierDetailsResponse = mapSupplierDetails(supplier);
            allSupplierDetailsResponse.getSupplierDetails().add(SupplierDetailsResponse.getSupplierDetails());
        }
        return allSupplierDetailsResponse;
    }


    @PayloadRoot(namespace = "http://rca_stock_mis.rca.com/suppliers", localPart = "CreateSupplierDetailsRequest")
    @ResponsePayload
    public CreateSupplierDetailsResponse save(@RequestPayload CreateSupplierDetailsRequest request) {
        Supplier supplier = supplierRepository.findById((long) request.getSupplierDetails().getId()).get();
        supplierRepository.save(new Supplier((long) request.getSupplierDetails().getId(),
                request.getSupplierDetails().getNames(),
                request.getSupplierDetails().getEmail(),
                request.getSupplierDetails().getMobile()


        ));
        CreateSupplierDetailsResponse SupplierDetailsResponse = new CreateSupplierDetailsResponse();
        SupplierDetailsResponse.setSupplierDetails(request.getSupplierDetails());
        SupplierDetailsResponse.setMessage("Created Successfully");
        return SupplierDetailsResponse;
    }

    @PayloadRoot(namespace = "http://rca_stock_mis.rca.com/suppliers", localPart = "UpdateSupplierDetailsRequest")
    @ResponsePayload
    public UpdateSupplierDetailsResponse update(@RequestPayload UpdateSupplierDetailsRequest request) {
        UpdateSupplierDetailsResponse SupplierDetailsResponse = null;
        Optional<Supplier> existingSupplier = this.supplierRepository.findById((long) request.getSupplierDetails().getId());
        if(existingSupplier.isEmpty() || existingSupplier == null) {
            SupplierDetailsResponse = mapSupplierDetail(null, "Id not found");
        }
        if(existingSupplier.isPresent()) {

            Supplier _supplier = existingSupplier.get();
            _supplier.setNames(request.getSupplierDetails().getNames());
            SupplierDetailsResponse = mapSupplierDetail(_supplier, "Updated successfully");

        }
        return SupplierDetailsResponse;
    }

    @PayloadRoot(namespace = "http://rca_stock_mis.rca.com/suppliers", localPart = "DeleteSupplierDetailsRequest")
    @ResponsePayload
    public DeleteSupplierDetailsResponse delete(@RequestPayload DeleteSupplierDetailsRequest request) {

        System.out.println("ID: "+request.getId());
        supplierRepository.deleteById((long) request.getId());

        DeleteSupplierDetailsResponse SupplierDetailsResponse = new DeleteSupplierDetailsResponse();
        SupplierDetailsResponse.setMessage("Deleted Successfully");
        return SupplierDetailsResponse;
    }

    private GetSupplierDetailsResponse mapSupplierDetails(Supplier supplier){
        SupplierDetails SupplierDetails = mapSupplier(supplier);

        GetSupplierDetailsResponse SupplierDetailsResponse = new GetSupplierDetailsResponse();

        SupplierDetailsResponse.setSupplierDetails(SupplierDetails);
        return SupplierDetailsResponse;
    }

    private UpdateSupplierDetailsResponse mapSupplierDetail(Supplier supplier, String message) {
        SupplierDetails SupplierDetails = mapSupplier(supplier);
        UpdateSupplierDetailsResponse SupplierDetailsResponse = new UpdateSupplierDetailsResponse();

        SupplierDetailsResponse.setSupplierDetails(SupplierDetails);
        SupplierDetailsResponse.setMessage(message);
        return SupplierDetailsResponse;
    }

    private SupplierDetails mapSupplier(Supplier supplier){
        SupplierDetails SupplierDetails = new SupplierDetails();
        SupplierDetails.setId(supplier.getId());
        SupplierDetails.setNames(supplier.getNames());
        SupplierDetails.setEmail(supplier.getEmail());
        SupplierDetails.setMobile(supplier.getPhone());
        return SupplierDetails;
    }
}
