package com.rca.rca_stock_mis.soap.endpoints;

import com.rca.rca_stock_mis.items.*;
import com.rca.rca_stock_mis.soap.bean.Item;
import com.rca.rca_stock_mis.soap.bean.Supplier;
import com.rca.rca_stock_mis.soap.enums.StatusEnum;
import com.rca.rca_stock_mis.soap.repository.IItemRepository;
import com.rca.rca_stock_mis.soap.repository.ISupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;
import java.util.Optional;
@Endpoint
public class ItemEndPoint {

    @Autowired
    private IItemRepository itemRepository;
    private ISupplierRepository supplierRepository;

    @PayloadRoot(namespace = "http://rca_stock_mis.rca.com/items", localPart = "GetItemDetailsRequest")
    @ResponsePayload
    public GetItemDetailsResponse findById(@RequestPayload GetItemDetailsRequest request) {

        Item item = itemRepository.findById((long) request.getId()).get();

        GetItemDetailsResponse ItemDetailsResponse = mapItemDetails(item);
        return  ItemDetailsResponse;
    }

    @PayloadRoot(namespace = "http://rca_stock_mis.rca.com/items", localPart = "GetAllItemDetailsRequest")
    @ResponsePayload
    public GetAllItemDetailsResponse findAll(@RequestPayload GetAllItemDetailsRequest request){
        GetAllItemDetailsResponse allItemDetailsResponse = new GetAllItemDetailsResponse();

        List<Item> items = itemRepository.findAll();
        for (Item item: items){
            GetItemDetailsResponse ItemDetailsResponse = mapItemDetails(item);
            allItemDetailsResponse.getItemDetails().add(ItemDetailsResponse.getItemDetails());
        }
        return allItemDetailsResponse;
    }


    @PayloadRoot(namespace = "http://rca_stock_mis.rca.com/items", localPart = "CreateItemDetailsRequest")
    @ResponsePayload
    public CreateItemDetailsResponse save(@RequestPayload CreateItemDetailsRequest request) {
        Supplier supplier = supplierRepository.findById((long) request.getItemDetails().getSupplierId()).get();

        StatusEnum status = StatusEnum.valueOf(request.getItemDetails().getStatus());
        itemRepository.save(new Item(request.getItemDetails().getId(),
                request.getItemDetails().getName(),
                request.getItemDetails().getItemCode(),
                status,
                request.getItemDetails().getPrice(),
                supplier


        ));
        CreateItemDetailsResponse ItemDetailsResponse = new CreateItemDetailsResponse();
        ItemDetailsResponse.setItemDetails(request.getItemDetails());
        ItemDetailsResponse.setMessage("Created Successfully");
        return ItemDetailsResponse;
    }

    @PayloadRoot(namespace = "http://rca_stock_mis.rca.com/items", localPart = "UpdateItemDetailsRequest")
    @ResponsePayload
    public UpdateItemDetailsResponse update(@RequestPayload UpdateItemDetailsRequest request) {
        UpdateItemDetailsResponse ItemDetailsResponse = null;
        Optional<Item> existingItem = this.itemRepository.findById((long) request.getItemDetails().getId());
        if(existingItem.isEmpty() || existingItem == null) {
            ItemDetailsResponse = mapItemDetail(null, "Id not found");
        }
        if(existingItem.isPresent()) {

            Item _item = existingItem.get();
            _item.setName(request.getItemDetails().getName());
            ItemDetailsResponse = mapItemDetail(_item, "Updated successfully");

        }
        return ItemDetailsResponse;
    }

    @PayloadRoot(namespace = "http://rca_stock_mis.rca.com/items", localPart = "DeleteItemDetailsRequest")
    @ResponsePayload
    public DeleteItemDetailsResponse delete(@RequestPayload DeleteItemDetailsRequest request) {

        System.out.println("ID: "+request.getId());
        itemRepository.deleteById((long) request.getId());

        DeleteItemDetailsResponse ItemDetailsResponse = new DeleteItemDetailsResponse();
        ItemDetailsResponse.setMessage("Deleted Successfully");
        return ItemDetailsResponse;
    }

    private GetItemDetailsResponse mapItemDetails(Item item){
        ItemDetails ItemDetails = mapItem(item);

        GetItemDetailsResponse ItemDetailsResponse = new GetItemDetailsResponse();

        ItemDetailsResponse.setItemDetails(ItemDetails);
        return ItemDetailsResponse;
    }

    private UpdateItemDetailsResponse mapItemDetail(Item item, String message) {
        ItemDetails ItemDetails = mapItem(item);
        UpdateItemDetailsResponse ItemDetailsResponse = new UpdateItemDetailsResponse();

        ItemDetailsResponse.setItemDetails(ItemDetails);
        ItemDetailsResponse.setMessage(message);
        return ItemDetailsResponse;
    }

    private ItemDetails mapItem(Item item){
        ItemDetails ItemDetails = new ItemDetails();
        ItemDetails.setPrice(item.getPrice());
        ItemDetails.setId(item.getId());
        ItemDetails.setName(item.getName());
        return ItemDetails;
    }
}
